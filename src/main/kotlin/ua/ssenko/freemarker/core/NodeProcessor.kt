package ua.ssenko.freemarker.core

import freemarker.core.TemplateElement
import ua.ssenko.freemarker.exception.CompilationException
import ua.ssenko.freemarker.utils.trimMultilineStartSpaces
import ua.ssenko.freemarker.utils.verifyFieldsClass
import ua.ssenko.freemarker.utils.verifyVersion
import java.lang.reflect.ParameterizedType
import java.util.*

/**
 * Created by Сегрей on 11.07.2016.
 */
abstract class NodeProcessor<T: TemplateElement> {

    protected abstract fun process(context: CompilationContext, node: T)

    final fun processNode(context: CompilationContext, node: T) {
        try {
            verify(node)
            context += "/* ${node.nodeName}:[${node.beginLine}:${node.beginColumn}|${node.endLine}:${node.endColumn}]*/"
            process(context, node)
            context += "/* End ${node.nodeName} */"
        } catch (e : Exception) {
            val message = "Compilation error: ${node.beginLine}:${node.beginColumn} ${node.getSource()}. Message: ${e.message}";
            throw CompilationException(message, e)
        }
    }

    abstract fun getFields() : Set<String>

    fun verify(node: T) {
        val fields = HashSet<String>();

        fields.add("INITIAL_REGULATED_CHILD_BUFFER_CAPACITY")
        fields.add("parent")
        fields.add("nestedBlock")
        fields.add("regulatedChildBuffer")
        fields.add("regulatedChildCount")
        fields.add("index")
        fields.add("template")
        fields.add("beginColumn")
        fields.add("beginLine")
        fields.add("endColumn")
        fields.add("endLine")
        fields.add("RUNTIME_EVAL_LINE_DISPLACEMENT")

        //node.verifyVersion(fields);
        fields.addAll(getFields())
        node.verifyVersion(fields);

    }

    fun getNodeClass() : Class<T> {
        return (javaClass.genericSuperclass as ParameterizedType).getActualTypeArguments()[0] as Class<T>
    }


}