package ua.ssenko.freemarker.core

import freemarker.core.Expression
import freemarker.core.TemplateElement
import freemarker.template.Template
import ua.ssenko.freemarker.exception.UnsupportedNodeException
import ua.ssenko.freemarker.utils.trimMultilineStartSpaces
import java.util.*

const val CODE_SPACE : String = "    "
const val DEFAULT : String = "${"$"}DEFAULT"
const val LOCAL : String = "${"$"}LOCAL"
const val GLOBAL : String = "${"$"}GLOBAL"

/**
 * Created by Сегрей on 10.07.2016.
 */
class CompilationContext {

    private var level = 0

    private var out : StringBuilder = StringBuilder()

    private val contextStack = LinkedList<StringBuilder>()

    private val templateProcessors = CompilerConfig().templateProcessors
    private val expressionProcessors = CompilerConfig().expressionProcessors

    var currentNameSpace: String = DEFAULT

    val variables = HashSet<String>()
    val globalVariables = HashSet<String>()
    val localVariables = HashSet<String>()

    val template: Template;
    val debugMode: Boolean;

    constructor(template: Template, debugMode: Boolean = false) {
        this.template = template;
        this.debugMode = debugMode;
    }


    fun isDebugMode(): Boolean {
        return debugMode;
    }

    fun pushOutContext() {
        contextStack.push(out)
        out = StringBuilder()
    }

    fun popOutContext() : StringBuilder {
        val tmpOut = out
        out = contextStack.pop()
        return tmpOut
    }

    fun incLevel() = level++;

    fun decLevel() = level--;

    fun getCode() : String = out.toString();

    fun appendCode(incode : String) {
        val code = incode.trimMultilineStartSpaces();
        code.lines().forEach {
            out.append(CODE_SPACE.repeat(level)).append(it).append("\n")
        }
    }

    fun getProcessor(node: TemplateElement) : NodeProcessor<TemplateElement> {
        val processor = this.templateProcessors.get(node.javaClass)
        if (processor == null) {
            throw UnsupportedNodeException(node.nodeName)
        }
        return processor
    }

    fun getProcessor(expression: Expression) : ExpressionProcessor<Expression> {
        val processor = this.expressionProcessors.get(expression.javaClass)
        if (processor == null) {
            throw UnsupportedNodeException("${expression.canonicalForm} ${expression.javaClass}")
        }
        return processor
    }

    operator fun plusAssign(code: String) {
        appendCode(code);
    }

    operator fun plus(code: String) : CompilationContext {
        appendCode(code);
        return this;
    }

}