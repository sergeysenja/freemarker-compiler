package ua.ssenko.freemarker.core

import freemarker.core.Expression
import freemarker.core.TemplateElement
import org.reflections.Reflections
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
class CompilerConfig {

    final val templateProcessors = HashMap<Class<TemplateElement>, NodeProcessor<TemplateElement>>();
    final val expressionProcessors = HashMap<Class<Expression>, ExpressionProcessor<Expression>>();

    constructor() {

        initTemplateProcessors();
        initExpressionProcessor();

    }

    fun initTemplateProcessors() {
        val reflections = Reflections()
        val processorClases = reflections.getSubTypesOf(NodeProcessor::class.java)
        processorClases.forEach {
            val instance = it.newInstance();
            val processor = instance as NodeProcessor<TemplateElement>
            templateProcessors.put(processor.getNodeClass(), processor);
        }
    }

    fun initExpressionProcessor() {
        val reflections = Reflections()
        val processorClases = reflections.getSubTypesOf(ExpressionProcessor::class.java)
        processorClases.forEach {
            val instance = it.newInstance();
            val processor = instance as ExpressionProcessor<Expression>
            expressionProcessors.put(processor.getExpressionClass(), processor);
        }
    }


}