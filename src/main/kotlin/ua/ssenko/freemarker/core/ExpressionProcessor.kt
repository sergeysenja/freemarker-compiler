package ua.ssenko.freemarker.core

import freemarker.core.Expression
import freemarker.core.TemplateElement
import ua.ssenko.freemarker.exception.CompilationException
import ua.ssenko.freemarker.utils.verifyVersion
import java.lang.reflect.ParameterizedType
import java.util.*

/**
 * Created by ssenko on 7/28/16.
 */
abstract class ExpressionProcessor<T: Expression> {

    protected abstract fun process(context: CompilationContext, expression: T)

    final fun processExpression(context: CompilationContext, expression: T) {
        try {
            verify(expression)
            context += "/* ${expression.canonicalForm}:[${expression.beginLine}:${expression.beginColumn}|${expression.endLine}:${expression.endColumn}]*/"
            process(context, expression)
            context += "/* End ${expression.canonicalForm} */"
        } catch (e : Exception) {
            val message = "Compilation error: ${expression.beginLine}:${expression.beginColumn} ${expression.getSource()}. Message: ${e.message}";
            throw CompilationException(message, e)
        }
    }

    abstract fun getFields() : Set<String>

    fun verify(expression: T) {
        val fields = HashSet<String>();

        fields.add("template")
        fields.add("beginColumn")
        fields.add("beginLine")
        fields.add("endColumn")
        fields.add("endLine")

        fields.add("constantValue")
        fields.add("RUNTIME_EVAL_LINE_DISPLACEMENT")


        //node.verifyVersion(fields);
        fields.addAll(getFields())
        expression.verifyVersion(fields);

    }

    fun getExpressionClass() : Class<T> {
        return (javaClass.genericSuperclass as ParameterizedType).getActualTypeArguments()[0] as Class<T>
    }


}