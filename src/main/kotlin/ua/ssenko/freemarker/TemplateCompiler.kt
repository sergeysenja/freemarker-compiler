package ua.ssenko.freemarker

import freemarker.template.Template
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.DEFAULT
import ua.ssenko.freemarker.core.GLOBAL
import ua.ssenko.freemarker.core.LOCAL
import ua.ssenko.freemarker.exception.UnsupportedNodeException
import ua.ssenko.freemarker.utils.getJavaName
import ua.ssenko.freemarker.utils.trimMultilineStartSpaces

/**
 * Created by Сегрей on 10.07.2016.
 */
class TemplateCompiler {

    val debugMode = false;

    fun compile(template: Template) : String {
        val context = CompilationContext(template)
        context.incLevel()
        context.incLevel()
        context.getProcessor(template.rootTreeNode).processNode(context, template.rootTreeNode)

        return generateJavaClass(template, context)
    }

    private fun generateJavaClass(template: Template, body: CompilationContext) : String {

        var javaClass = """

        import java.util.ArrayList;
        import java.util.HashMap;
        import java.util.Map;

        public class ${template.getJavaName()} {

            public String generate() {
                final State ${DEFAULT} = new State();
                final Locals ${LOCAL} = new Locals();
                final Globals ${GLOBAL} = new Globals();
                final StringBuilder out = new StringBuilder();

        """.trimMultilineStartSpaces()

        javaClass += body.getCode()

        javaClass += """
        //
                return out.toString();

            }
        """.trimMultilineStartSpaces()

        javaClass += generateState(body.variables, "State")
        javaClass += generateState(body.localVariables, "Locals")
        javaClass += generateState(body.globalVariables, "Globals")

        javaClass += """
        }
        """.trimMultilineStartSpaces()


        return javaClass;
    }

    private fun generateState(variables: Set<String>, name: String): String {
        return """
        //
            public static class ${name} {
                private ArrayList<Object> vars = new ArrayList<>(${variables.size});

                ${generateVariableIndexes(variables)}

                private Map<String, Integer> varsId = new HashMap<>(${variables.size});

                {
                    ${generateVariablesMap(variables)}
                }
            }
        """.trimMultilineStartSpaces();
    }

    private fun generateVariableIndexes(variables: Set<String>) : String {
        val sb = StringBuilder();
        var i = 0;
        variables.forEach {
            sb.append("private static int ${it} = ${i};").append("\n").append("                ");
            ++i;
        }
        return sb.toString();
    }

    private fun generateVariablesMap(variables: Set<String>) : String {
        val sb = StringBuilder();
        var i = 0;
        variables.forEach {
            sb.append("varsId.put(\"${it}\", ${i});").append("\n").append("                    ");
            ++i;
        }
        return sb.toString();
    }

}

