package ua.ssenko.freemarker.utils

import freemarker.core.TemplateElement
import freemarker.core.TemplateObject
import freemarker.template.Configuration
import freemarker.template.DefaultObjectWrapper
import freemarker.template.Template
import org.apache.commons.lang3.StringEscapeUtils
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.utils.exception.VerificationException
import java.io.File
import java.io.IOException
import java.io.StringReader
import java.nio.CharBuffer
import java.util.*

/**
 * Created by Сегрей on 10.07.2016.
 */
fun String.trimMultilineStartSpaces(): String {
    val template = StringBuilder();
    val tabPadding: Int = this.lines().map(String::countStartSpaces).min() ?: 0;

    this.lines().map { str -> trimStartSpace(str, tabPadding) }.forEach { str -> template.append(str).append("\n") };
    return template.toString();
}


inline fun Any?.toJavaCode(): String = StringEscapeUtils.escapeJava(castToString())

inline fun Any?.castToString(): String = if (this is CharArray) {
    String(this)
} else {
    this!!.toString()
}

private fun trimStartSpace(str: String, tabPadding: Int): String = if (str.length > tabPadding) str.substring(tabPadding) else str;

fun String.countStartSpaces(): Int {
    if (this.isBlank()) {
        return Int.MAX_VALUE;
    }

    var countSpaces = 0;
    for (index in this.indices) {
        if (this[index].isWhitespace()) {
            countSpaces++;
        } else {
            break;
        }
    }
    return countSpaces;
}

fun TemplateObject.verifyVersion(fields: Set<String>) {
    for (field in this.getAllField()) {
        if (!fields.contains(field.getName())) {
            throw VerificationException(field.getName())
        }
    }
}

fun Any.verifyFieldsClass(fields: Set<String>) {
    for (field in this.javaClass.declaredFields) {
        if (!fields.contains(field.name)) {
            throw VerificationException(field.name)
        }
    }
    if (fields.size !== this.javaClass.getDeclaredFields().size) {
        throw VerificationException("size not equals")
    }
}

operator fun StringBuilder.plusAssign(code: String) {
    append(code);
}

operator fun StringBuilder.plus(code: String) : StringBuilder {
    append(code);
    return this;
}

fun toSet(vararg strings: String): Set<String> {
    val rez = HashSet<String>()
    for (el in strings) {
        rez.add(el)
    }
    return rez
}


fun Template.getJavaName(): String {
    val javaName = StringBuilder()

    val hash = hashToString(this.sourceName.hashCode())
    val templateId = this.name + "$" + hash.toString()
    for ((index, c) in templateId.toCharArray().withIndex()) {
        if (c.isJavaIdentifierStart()) {
            javaName.append(c)
            templateId.substring(index + 1).filter { it.isJavaIdentifierPart() }.forEach {
                javaName.append(it)
            }
            break
        }
    }

    return javaName.toString()
}

private val mapHash = mapOf(Pair('1', "q"), Pair('2', "w"), Pair('3', "e"), Pair('4', "r"), Pair('5', "t"),
        Pair('6', "y"), Pair('7', "u"), Pair('8', "i"), Pair('9', "o"), Pair('0', "p"), Pair('-', "M"))
fun hashToString(hashCode: Int): String {

    val hash = StringBuilder()

    hashCode.toString().toCharArray().forEach {
        hash.append(mapHash[it])
    }
    return hash.toString()
}

