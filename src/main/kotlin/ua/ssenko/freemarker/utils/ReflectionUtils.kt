package ua.ssenko.freemarker.utils

import org.reflections.ReflectionUtils
import java.lang.reflect.Field

/**
 * Created by Сегрей on 11.07.2016.
 */
fun Any.setPrivateField(fieldName: String, value: Any) {
    val filed = findField(this.javaClass, fieldName)
    filed!!.isAccessible = true
    setField(filed, value)
}

fun Any.setField(field: Field, value: Any) {
    try {
        field.set(this, value)
    } catch (ex: IllegalAccessException) {
        throw IllegalStateException(
                "Unexpected reflection exception - " + ex.javaClass.name + ": " + ex.message)
    }

}

fun Any.callMethod(name: String): Any {
    val method = this.javaClass.getDeclaredMethod(name)
    method.setAccessible(true)
    val startTime1 = System.nanoTime()
    val result = method.invoke(this)
    val endTime1 = System.nanoTime()
    println("compiler time : |${(endTime1 - startTime1)/1000}|")
    return result
}

fun Any.getPrivateField(fieldName: String): Any? {
    val filed = findField(this.javaClass, fieldName)
    filed!!.isAccessible = true
    return this.getField(filed)
}

private fun findField(clazz: Class<*>, name: String, type: Class<*>? = null): Field? {
    var searchType: Class<*>? = clazz
    while (Any::class.java != searchType && searchType != null) {
        val fields = searchType.declaredFields
        for (field in fields) {
            if ((name == null || name == field.name) && (type == null || type == field.type)) {
                return field
            }
        }
        searchType = searchType.superclass
    }
    return null
}

fun Any.getField(field: Field): Any? {
    try {
        return field.get(this)
    } catch (ex: IllegalAccessException) {
        throw IllegalStateException(
                "Unexpected reflection exception - " + ex.javaClass.name + ": " + ex.message)
    }

}


private fun concat(a: Array<Field?>, b: Array<Field>): Array<Field?> {
    val aLen = a.size
    val bLen = b.size
    val c = arrayOfNulls<Field>(aLen + bLen)
    System.arraycopy(a, 0, c, 0, aLen)
    System.arraycopy(b, 0, c, aLen, bLen)
    return c
}

fun Any.getAllField(): List<Field> {
    var searchType: Class<*>? = this.javaClass
    var result = arrayOfNulls<Field>(0)
    while (Any::class.java != searchType && searchType != null) {
        val fields = searchType.declaredFields
        result = concat(result, fields)
        searchType = searchType.superclass
    }
    val rez = result.filterNotNull();
    return rez
}
