package ua.ssenko.freemarker.exception

/**
 * Created by ssenko on 7/28/16.
 */
class UnsupportedExpressionException(message: String) : RuntimeException(message) {
}