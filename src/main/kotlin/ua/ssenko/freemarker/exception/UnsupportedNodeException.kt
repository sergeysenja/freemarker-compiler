package ua.ssenko.freemarker.exception

/**
 * Created by ssenko on 7/22/16.
 */
class UnsupportedNodeException(message: String) : RuntimeException(message) {

}