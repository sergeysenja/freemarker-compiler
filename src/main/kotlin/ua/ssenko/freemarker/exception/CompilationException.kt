package ua.ssenko.freemarker.exception

/**
 * Created by ssenko on 7/25/16.
 */
class CompilationException(message: String, e: Throwable) : RuntimeException(message, e) {
}