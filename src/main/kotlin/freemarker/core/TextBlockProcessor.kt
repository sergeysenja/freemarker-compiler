
package freemarker.core

import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import ua.ssenko.freemarker.utils.getPrivateField
import ua.ssenko.freemarker.utils.toJavaCode
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class TextBlockProcessor : NodeProcessor<TextBlock>() {

    override fun process(context: CompilationContext, node: TextBlock) {
        context += """
            out.append("${node.getPrivateField("text").toJavaCode()}");
        """
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("EMPTY_CHAR_ARRAY");
    
        fields.add("EMPTY_BLOCK");
    
        fields.add("text");
    
        fields.add("unparsed");
    
        return fields;
    }

}

