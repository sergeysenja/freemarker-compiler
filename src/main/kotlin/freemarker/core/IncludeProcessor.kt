
package freemarker.core

import freemarker.core.Include
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class IncludeProcessor : NodeProcessor<Include>() {

    override fun process(context: CompilationContext, node: Include) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("includedTemplateNameExp");
    
        fields.add("encodingExp");
    
        fields.add("parseExp");
    
        fields.add("ignoreMissingExp");
    
        fields.add("encoding");
    
        fields.add("parse");
    
        fields.add("ignoreMissingExpPrecalcedValue");
    
        return fields;
    }

}
            
