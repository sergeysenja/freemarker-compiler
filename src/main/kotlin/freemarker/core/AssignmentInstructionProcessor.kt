
package freemarker.core

import freemarker.core.AssignmentInstruction
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import ua.ssenko.freemarker.utils.getPrivateField
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class AssignmentInstructionProcessor : NodeProcessor<AssignmentInstruction>() {

    override fun process(context: CompilationContext, node: AssignmentInstruction) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("scope");
    
        fields.add("namespaceExp");
    
        return fields;
    }

}
            
