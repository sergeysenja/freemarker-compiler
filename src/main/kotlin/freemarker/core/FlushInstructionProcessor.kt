
package freemarker.core

import freemarker.core.FlushInstruction
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class FlushInstructionProcessor : NodeProcessor<FlushInstruction>() {

    override fun process(context: CompilationContext, node: FlushInstruction) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        return fields;
    }

}
            
