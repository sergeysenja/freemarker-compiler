
package freemarker.core

import freemarker.core.IteratorBlock
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class IteratorBlockProcessor : NodeProcessor<IteratorBlock>() {

    override fun process(context: CompilationContext, node: IteratorBlock) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("listExp");
    
        fields.add("loopVarName");
    
        fields.add("isForEach");
    
        return fields;
    }

}
            
