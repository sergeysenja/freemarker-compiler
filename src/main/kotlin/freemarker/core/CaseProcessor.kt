
package freemarker.core

import freemarker.core.Case
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class CaseProcessor : NodeProcessor<Case>() {

    override fun process(context: CompilationContext, node: Case) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("TYPE_CASE");
    
        fields.add("TYPE_DEFAULT");
    
        fields.add("condition");
    
        return fields;
    }

}
            
