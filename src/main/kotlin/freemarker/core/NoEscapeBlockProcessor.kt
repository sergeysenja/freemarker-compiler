
package freemarker.core

import freemarker.core.NoEscapeBlock
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class NoEscapeBlockProcessor : NodeProcessor<NoEscapeBlock>() {

    override fun process(context: CompilationContext, node: NoEscapeBlock) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        return fields;
    }

}
            
