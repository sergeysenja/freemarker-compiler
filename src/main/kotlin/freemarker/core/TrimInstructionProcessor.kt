
package freemarker.core

import freemarker.core.TrimInstruction
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class TrimInstructionProcessor : NodeProcessor<TrimInstruction>() {

    override fun process(context: CompilationContext, node: TrimInstruction) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("TYPE_T");
    
        fields.add("TYPE_LT");
    
        fields.add("TYPE_RT");
    
        fields.add("TYPE_NT");
    
        fields.add("left");
    
        fields.add("right");
    
        return fields;
    }

}
            
