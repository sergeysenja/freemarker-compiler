
package freemarker.core

import freemarker.core.Comment
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import ua.ssenko.freemarker.utils.getPrivateField
import ua.ssenko.freemarker.utils.toJavaCode
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class CommentProcessor : NodeProcessor<Comment>() {

    override fun process(context: CompilationContext, node: Comment) {
        val comment = "//" + node.getPrivateField("text")?.toJavaCode()
        context += comment
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("text");
    
        return fields;
    }

}
            
