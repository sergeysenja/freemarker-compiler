
package freemarker.core

import freemarker.core.ReturnInstruction
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class ReturnInstructionProcessor : NodeProcessor<ReturnInstruction>() {

    override fun process(context: CompilationContext, node: ReturnInstruction) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("exp");
    
        return fields;
    }

}
            
