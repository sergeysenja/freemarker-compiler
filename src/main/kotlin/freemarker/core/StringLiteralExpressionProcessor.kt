package freemarker.core

import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.ExpressionProcessor
import ua.ssenko.freemarker.utils.exception.VerificationException
import ua.ssenko.freemarker.utils.getPrivateField
import ua.ssenko.freemarker.utils.toJavaCode
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class StringLiteralExpressionProcessor : ExpressionProcessor<StringLiteral>() {

    override fun process(context: CompilationContext, expression: freemarker.core.StringLiteral) {
        val dynamicValue = expression.getPrivateField("dynamicValue")
        if (dynamicValue == null) {
            if (expression.getPrivateField("value") == null) {
                throw NullPointerException("value is null");
            } else {
                context += """
                    out.append("${expression.getPrivateField("value").toJavaCode()}");
                """
            }
        } else {
            if (dynamicValue is TemplateElement) {
                context.getProcessor(dynamicValue).processNode(context, dynamicValue)
            } else {
                throw VerificationException("dynamicValue is not TemplateElement | ${dynamicValue.javaClass.simpleName}")
            }
        }
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();

        fields.add("value");

        fields.add("dynamicValue");

        return fields;
    }

}