
package freemarker.core

import freemarker.core.Items
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class ItemsProcessor : NodeProcessor<Items>() {

    override fun process(context: CompilationContext, node: Items) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("loopVarName");
    
        return fields;
    }

}
            
