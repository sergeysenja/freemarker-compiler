
package freemarker.core

import freemarker.core.EscapeBlock
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class EscapeBlockProcessor : NodeProcessor<EscapeBlock>() {

    override fun process(context: CompilationContext, node: EscapeBlock) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("variable");
    
        fields.add("expr");
    
        fields.add("escapedExpr");
    
        return fields;
    }

}
            
