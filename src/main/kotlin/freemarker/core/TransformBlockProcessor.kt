
package freemarker.core

import freemarker.core.TransformBlock
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class TransformBlockProcessor : NodeProcessor<TransformBlock>() {

    override fun process(context: CompilationContext, node: TransformBlock) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("transformExpression");
    
        fields.add("namedArgs");
    
        fields.add("sortedNamedArgsCache");
    
        fields.add("class${"$"}freemarker${"$"}template${"$"}TemplateTransformModel");
    
        return fields;
    }

}
            
