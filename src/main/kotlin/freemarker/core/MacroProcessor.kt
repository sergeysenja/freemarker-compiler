
package freemarker.core

import freemarker.core.Macro
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class MacroProcessor : NodeProcessor<Macro>() {

    override fun process(context: CompilationContext, node: Macro) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("DO_NOTHING_MACRO");
    
        fields.add("TYPE_MACRO");
    
        fields.add("TYPE_FUNCTION");
    
        fields.add("name");
    
        fields.add("paramNames");
    
        fields.add("paramDefaults");
    
        fields.add("catchAllParamName");
    
        fields.add("function");
    
        return fields;
    }

}
            
