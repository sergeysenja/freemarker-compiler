
package freemarker.core

import freemarker.core.AttemptBlock
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class AttemptBlockProcessor : NodeProcessor<AttemptBlock>() {

    override fun process(context: CompilationContext, node: AttemptBlock) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("attemptBlock");
    
        fields.add("recoveryBlock");
    
        return fields;
    }

}
            
