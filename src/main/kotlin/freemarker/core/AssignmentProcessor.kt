
package freemarker.core

import freemarker.core.Assignment
import freemarker.template.TemplateModel
import freemarker.template.TemplateNumberModel
import freemarker.template.TemplateScalarModel
import ua.ssenko.freemarker.core.*
import ua.ssenko.freemarker.utils.callMethod
import ua.ssenko.freemarker.utils.getPrivateField
import ua.ssenko.freemarker.utils.plusAssign
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class AssignmentProcessor : NodeProcessor<Assignment>() {

    private val OPERATOR_TYPE_EQUALS = 0x10000
    private val OPERATOR_TYPE_PLUS_EQUALS = 0x10001
    private val OPERATOR_TYPE_PLUS_PLUS = 0x10002
    private val OPERATOR_TYPE_MINUS_MINUS = 0x10003

    override fun process(context: CompilationContext, node: Assignment) {

        val namespaceExp = node.getPrivateField("namespaceExp")
        val scope = node.getPrivateField("scope")
        val variableName = node.getPrivateField("variableName").toString()
        val operatorType = node.getPrivateField("operatorType")
        val valueExp = node.getPrivateField("valueExp")

        context += "if (true) { //need to isolate java namespace"

        context.incLevel()

        println("scope: ${scope}")
        println("variableName: ${variableName}")
        println("operatorType: ${operatorType}")
        println("operatorType: ${node.callMethod("getOperatorTypeAsString")}")
        println("valueExp: ${valueExp}")
        println("namespaceExp: ${namespaceExp}")


        if (OPERATOR_TYPE_EQUALS.equals(operatorType)) {
            context += "Object assignmentValue = " + context.getProcessor(valueExp as Expression).processExpression(context, valueExp)
        } else {
            var lhoValue: TemplateModel?
            if (namespace == null) {
                lhoValue = env.getLocalVariable(variableName)
            } else {
                lhoValue = namespace.get(variableName)
            }

            if (operatorType == OPERATOR_TYPE_PLUS_EQUALS) {  // Add or concat operation
                if (lhoValue == null) {
                    if (env.isClassicCompatible()) {
                        lhoValue = freemarker.template.TemplateScalarModel.EMPTY_STRING
                    } else {
                        throw InvalidReferenceException.getInstance(
                                variableName, getOperatorTypeAsString(), env)
                    }
                }

                value = valueExp.eval(env)
                if (value == null) {
                    if (env.isClassicCompatible()) {
                        value = freemarker.template.TemplateScalarModel.EMPTY_STRING
                    } else {
                        throw InvalidReferenceException.getInstance(valueExp, env)
                    }
                }
                value = AddConcatExpression._eval(env, namespaceExp, null, lhoValue, valueExp, value)
            } else {  // Numerical operation
                val lhoNumber: Number
                if (lhoValue is TemplateNumberModel) {
                    lhoNumber = EvalUtil.modelToNumber((lhoValue as TemplateNumberModel?)!!, null)
                } else if (lhoValue == null) {
                    throw InvalidReferenceException.getInstance(variableName, getOperatorTypeAsString(), env)
                } else {
                    throw NonNumericalException(variableName, lhoValue, null, env)
                }

                if (operatorType == OPERATOR_TYPE_PLUS_PLUS) {
                    value = AddConcatExpression._evalOnNumbers(env, getParentElement(), lhoNumber, ONE)
                } else if (operatorType == OPERATOR_TYPE_MINUS_MINUS) {
                    value = ArithmeticExpression._eval(
                            env, getParentElement(), lhoNumber, ArithmeticExpression.TYPE_SUBSTRACTION, ONE)
                } else { // operatorType == ArithmeticExpression.TYPE_...
                    val rhoNumber = valueExp.evalToNumber(env)
                    value = ArithmeticExpression._eval(env, this, lhoNumber, operatorType, rhoNumber)
                }
            }
        }


        if (namespaceExp != null) {
            val error = "not implemented assign with namespaceExp | ${node.dump(true)}"
            throw UnsupportedOperationException(error)
        } else {
            when (scope) {
                Assignment.LOCAL -> {
                    context.localVariables.add(variableName)
                    context += "${LOCAL}.set(${LOCAL}.${variableName}, assignmentValue)"
                }
                Assignment.GLOBAL -> {
                    context.globalVariables.add(variableName)
                    context += "${GLOBAL}.set(${GLOBAL}.${variableName}, assignmentValue)"
                }
                Assignment.NAMESPACE -> {
                    context.variables.add(variableName)
                    context += "${context.currentNameSpace}.vars.set(${DEFAULT}.${variableName}, assignmentValue)"
                }
                else -> throw BugException("Unexpected scope type: " + scope)
            }
        }

        context.decLevel()
        context += "}"

    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("OPERATOR_TYPE_EQUALS");
    
        fields.add("OPERATOR_TYPE_PLUS_EQUALS");
    
        fields.add("OPERATOR_TYPE_PLUS_PLUS");
    
        fields.add("OPERATOR_TYPE_MINUS_MINUS");
    
        fields.add("scope");
    
        fields.add("variableName");
    
        fields.add("operatorType");
    
        fields.add("valueExp");
    
        fields.add("namespaceExp");
    
        fields.add("NAMESPACE");
    
        fields.add("LOCAL");
    
        fields.add("GLOBAL");
    
        fields.add("ONE");
    
        return fields;
    }

}
            
