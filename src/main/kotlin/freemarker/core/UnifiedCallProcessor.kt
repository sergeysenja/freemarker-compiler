
package freemarker.core

import freemarker.core.UnifiedCall
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class UnifiedCallProcessor : NodeProcessor<UnifiedCall>() {

    override fun process(context: CompilationContext, node: UnifiedCall) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("nameExp");
    
        fields.add("namedArgs");
    
        fields.add("positionalArgs");
    
        fields.add("bodyParameterNames");
    
        fields.add("legacySyntax");
    
        fields.add("sortedNamedArgsCache");
    
        fields.add("customDataHolder");
    
        return fields;
    }

}
            
