
package freemarker.core

import freemarker.core.SwitchBlock
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class SwitchBlockProcessor : NodeProcessor<SwitchBlock>() {

    override fun process(context: CompilationContext, node: SwitchBlock) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("defaultCase");
    
        fields.add("searched");
    
        return fields;
    }

}
            
