
package freemarker.core

import freemarker.core.ConditionalBlock
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class ConditionalBlockProcessor : NodeProcessor<ConditionalBlock>() {

    override fun process(context: CompilationContext, node: ConditionalBlock) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("TYPE_IF");
    
        fields.add("TYPE_ELSE");
    
        fields.add("TYPE_ELSE_IF");
    
        fields.add("condition");
    
        fields.add("type");
    
        fields.add("isLonelyIf");
    
        return fields;
    }

}
            
