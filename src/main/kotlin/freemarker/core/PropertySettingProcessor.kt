
package freemarker.core

import freemarker.core.PropertySetting
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class PropertySettingProcessor : NodeProcessor<PropertySetting>() {

    override fun process(context: CompilationContext, node: PropertySetting) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("key");
    
        fields.add("value");
    
        fields.add("SETTING_NAMES");
    
        return fields;
    }

}
            
