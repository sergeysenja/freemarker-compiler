
package freemarker.core

import freemarker.core.MixedContent
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class MixedContentProcessor : NodeProcessor<MixedContent>() {

    override fun process(context: CompilationContext, node: MixedContent) {
        for(i in 0..node.regulatedChildCount - 1) {
            val childNode = node.getRegulatedChild(i)
            context.getProcessor(childNode).processNode(context, childNode)
        }
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        return fields;
    }

}

