
package freemarker.core

import freemarker.core.BlockAssignment
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import ua.ssenko.freemarker.utils.getPrivateField
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class BlockAssignmentProcessor : NodeProcessor<BlockAssignment>() {

    override fun process(context: CompilationContext, node: BlockAssignment) {
        println(node.dump(true))
        println("scope ${node.getPrivateField("scope")}")
        println("varName ${node.getPrivateField("varName")}")
        println("namespaceExp ${node.getPrivateField("namespaceExp")}")
        //throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("varName");
    
        fields.add("namespaceExp");
    
        fields.add("scope");
    
        return fields;
    }

}
            
