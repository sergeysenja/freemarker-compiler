
package freemarker.core

import freemarker.core.RecurseNode
import ua.ssenko.freemarker.core.CompilationContext
import ua.ssenko.freemarker.core.NodeProcessor
import java.util.*

/**
 * Created by Сегрей on 17.07.2016.
 */
internal class RecurseNodeProcessor : NodeProcessor<RecurseNode>() {

    override fun process(context: CompilationContext, node: RecurseNode) {
        throw UnsupportedOperationException("not implemented")
    }

    override fun getFields(): Set<String> {
        val fields = HashSet<String>();
            
        fields.add("targetNode");
    
        fields.add("namespaces");
    
        return fields;
    }

}
            
