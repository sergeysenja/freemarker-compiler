package ua.ssenko.freemarker.utils;

import javax.tools.JavaCompiler;
import javax.tools.ToolProvider;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

/**
 * Created by ssenko on 7/25/16.
 */
public class JavaCodeCompiler {

    public static Object compileJava(String className, String source) throws Exception {
        // Prepare source somehow.

        source = "package test;\n" + source;

// Save source in .java file.
        File root = new File("/tmp/java");
        File sourceFile = new File(root, "test/" + className + ".java");
        sourceFile.getParentFile().mkdirs();
        Files.write(sourceFile.toPath(), source.getBytes(StandardCharsets.UTF_8));

// Compile source file.
        javax.tools.JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        //InputStream in, OutputStream out, OutputStream err
        compiler.run(System.in, System.out, System.err, sourceFile.getPath());

// Load and instantiate compiled class.
        URLClassLoader classLoader = URLClassLoader.newInstance(new URL[]{root.toURI().toURL()});
        Class<?> cls = Class.forName("test." + className, true, classLoader);
        return cls.newInstance();
    }

}
