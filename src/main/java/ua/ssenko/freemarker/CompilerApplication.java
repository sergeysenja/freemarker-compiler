package ua.ssenko.freemarker;

import java.io.File;
import java.io.IOException;
import java.io.StringReader;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Scanner;

import freemarker.template.Configuration;
import freemarker.template.DefaultObjectWrapper;

import freemarker.template.Template;
import freemarker.template.TemplateException;

public class CompilerApplication {

	public static void main(String[] args) throws IOException, TemplateException {

		Template symple = loadTemplate("somename", getFile("templates/symple.ftl"));
		TemplateCompiler compiler = new TemplateCompiler();
		System.out.println(compiler.compile(symple));
	}

	public static Template loadTemplate(String templateName, String source) throws IOException {
		Configuration cfg = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
		cfg.setObjectWrapper(new DefaultObjectWrapper(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS));

		File file = new File("./src/main/resources/templates/");

		cfg.setDirectoryForTemplateLoading(file);

		Template t = new Template(templateName, new StringReader(source), cfg);
		return t;
	}

	public static String getFile(String fileName) {

		StringBuilder result = new StringBuilder("");

		// Get file from resources folder
		ClassLoader classLoader = CompilerApplication.class.getClassLoader();
		File file = new File(classLoader.getResource(fileName).getFile());

		try (Scanner scanner = new Scanner(file)) {

			while (scanner.hasNextLine()) {
				String line = scanner.nextLine();
				result.append(line).append("\n");
			}

			scanner.close();

		} catch (IOException e) {
			throw new RuntimeException(e);
		}

		return result.toString();
	}


}
