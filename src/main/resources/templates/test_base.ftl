<#global contextPath = "contextPath" />
<#global baseUrl="baseUrl" />
<#global requestUrl="requestUrl" />
<#global langSuffix = "ru" />
<#assign baseUrl = baseUrl + "/" + langSuffix>
<#macro head>
  <script>
    var baseApp = angular.module('vantage.app',['ui-notifications','fixVisual']);
  </script>
</#macro>
<#macro scripts>
</#macro>
<#macro topmenu>
</#macro>

<#function tr code>
  <#assign defaultText>No transliteration for ${code}</#assign>
  <#assign message>
    <@spring.messageText code, defaultText />
  </#assign>
  <#return message />
</#function>

<#function if conditions iftrue iffalse>
  <#if conditions>
    <#return iftrue />
    <#else>
      <#return iffalse />
  </#if>
</#function>

<#function replaceQuotes instr>
  <#assign q1>${instr?replace("'", "\\'")}</#assign>
  <#assign q2>${q1?replace("{", "\\{")}</#assign>
  <#assign q3>${q2?replace("}", "\\}")}</#assign>
  <#return q3 />
</#function>

<#function round value roundModule>
  <#assign result>${(value/roundModule)?int*roundModule}</#assign>
  <#return result />
</#function>

<#setting boolean_format="true, false" />
<#function isStateIs objectStateName, stateName>
  <#-- if template failed this, add to mode static metadata. See GeneralUtilsComponent.addStaticContext -->
    <#assign result>${statics["ua.com.jbs.vantage.domain.converter.InheritanceAdvertStateConverter"].stateInstanceOf(objectStateName, stateName)}</#assign>
    <#assign result=result?boolean />
    <#return result />
</#function>

<#function isStateIsDeal objectStateName, stateName>
  <#-- if template failed this, add to mode static metadata. See GeneralUtilsComponent.addStaticContext -->
    <#assign result>${statics["ua.com.jbs.vantage.domain.converter.InheritanceDealStateConverter"].stateInstanceOf(objectStateName, stateName)}</#assign>
    <#assign result=result?boolean />
    <#return result />
</#function>

<#macro display_page>
  <!DOCTYPE html>
  <html lang="ru" ng-app="vantage.app">
  <#assign security=JspTaglibs[ "http://www.springframework.org/security/tags"] />
  <#import "/spring.ftl" as spring />
  <#setting number_format="#" />
  <#setting boolean_format="true, false" />

  <head>
    <title>
      <@title/>
    </title>
    <meta charset='utf-8'>
    <meta name="keywords" content="">
    <meta name="description" content="">
    <meta name="robots" content="index,follow">
    <meta name="format-detection" content="telephone=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="apple-mobile-web-app-status-bar-style" content="default">
    <meta name="_csrf" content="${_csrf.token}" />
    <meta name="_csrf_header" content="${_csrf.headerName}" />
    <link rel="shortcut icon" href="${baseUrl}/img/favicon.png">
    <link rel="stylesheet" href="${baseUrl}/bower_components/uikit/css/uikit.almost-flat.min.css">
    <link rel="stylesheet" href="${baseUrl}/css/main.min.css?2">
    <link rel="stylesheet" href="${baseUrl}/css/main.css?2">
    <script src="${baseUrl}/bower_components/angular/angular.min.js"></script>
    <@head/>
  </head>
  <body id="body" class="app_theme_e" ng-controller="baseCtrl">
    <div class="page-wrap">
      <header id="header_main">
        <nav class="uk-navbar main-header">
          <ul class="uk-navbar-nav ">
            <li><a href="${baseUrl}/">LOGO</a></li>
          </ul>
          <div class="uk-navbar-center">
            <ul class="uk-navbar-nav">
              <li>
                <div class="buttons">
                  <a href="#filters" data-uk-modal class="order" onClick="urlChange('${baseUrl}/search/orders')">Cargo</a>
                  <i class="mdi mdi-magnify"></i>
                  <a href="#filters" data-uk-modal class="offer" onClick="urlChange('${baseUrl}/search/offers')">Truck</a>
                </div>
              </li>
              <li><a href="${baseUrl}/">News</a></li>
              <li><a href="${baseUrl}/">Library</a></li>
              <@security.authorize access="isAuthenticated()">
                <li><a href="${baseUrl}/orders/active" id="my-office">My office</a></li>
              </@security.authorize>
              <li><a href="${baseUrl}/">Help</a></li>
            </ul>
          </div>
          <div class="uk-navbar-flip clients-info">
            <ul class="uk-navbar-nav">
              <@security.authorize access="isAuthenticated()">
                <!-- if new messages -->
                <li ng-cloak ng-controller="notifiCtrl" ng-init="unreadCounter = ${unreadNotificationCounter!0};" class="notofication-holder">
                  <a 
                    href="javascript:void(0)" 
                    ng-if="unreadCounter > 0"
                    ng-click="onShowUnread(true)"
                    class="notofications-btn"
                  >
                    <span class="new-msg uk-animation-shake">{{ unreadCounter }}</span>
                  </a>
                  <a 
                    href="javascript:void(0)" 
                    ng-if="unreadCounter == 0"
                    ng-click="onShowUnread(true)"
                    class="notofications-btn"
                  >
                    <i class="mdi mdi-bell"></i>
                  </a>
                  <#include "notifications/notifications-ui.ftl" />
                </li>

                <li class="uk-parent login" data-uk-dropdown="{mode:'click'}">
                  <a href=""><img src="${baseUrl}/img/avatar.svg" class="ava" alt=""></a>
                  <div class="uk-dropdown uk-dropdown-navbar">
                    <ul class="uk-nav uk-nav-navbar">
                      <li class="uk-text-center">
                        <strong><@security.authentication property="principal.username" /></strong>
                        <small id="user-status">
                          <@security.authorize access="hasRole('ROLE_NEW')">
                          NEW
                          </@security.authorize>
                          <@security.authorize access="hasRole('ROLE_CONFIRMED')">
                          CONFIRMED
                          </@security.authorize>
                          <@security.authorize access="hasRole('ROLE_VERIFIED')">
                          VERIFIED
                          </@security.authorize>
                        </small>
                      </li>
                      <li class="uk-nav-divider"></li>
                      <li class="uk-text-center">
                        <form action="${baseUrl}/logout" method="post">
                          <button id="logout" class="uk-button uk-button-primary">Sign out <i class="mdi mdi-logout"></i></button>
                          <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
                        </form>
                      </li>
                    </ul>
                  </div>
                </li>
              </@security.authorize>
              <@security.authorize access="isAnonymous()">
                <li>
                  <a href="${baseUrl}/login" id="login">Sign in <i class="mdi mdi-login"></i></a>
                </li>
              </@security.authorize>
            </ul>
          </div>
        </nav>
      </header>
      <@topmenu/>
      <div class="uk-container uk-container-center">
        <@body/>
      </div>
    </div>
    <footer id="footer" class="md-bg-blue-grey-900">
      <div class="uk-container uk-container-center">
        <div class="uk-grid">
          <div class="uk-width-1-4" >
            <h3>Quick settings</h3>
            <#assign c = .lang />
            <div ng-cloak>
              <select id="lang-session" data-md-selectize>
                <option value="${contextPath}/en${servletPath!}?${queryString!}" <#if c="en">selected</#if>>English</option>
                <option value="${contextPath}/ru${servletPath!}?${queryString!}" <#if c="ru">selected</#if>>Русский</option>
                <option value="${contextPath}/de${servletPath!}?${queryString!}" <#if c="de">selected</#if>>Deutch</option>
              </select>

              <#assign c = Session.quick_setting_currency!601 />
              <select id="currency-session" data-md-selectize>
                <option value="600" <#if c=600>selected</#if>>UAH</option>
                <option value="601" <#if c=601>selected</#if>>USD</option>
                <option value="602" <#if c=602>selected</#if>>EUR</option>
                <option value="603" <#if c=603>selected</#if>>RUR</option>
              </select>              
            </div>
          </div>
          <div class="uk-width-1-4">
            <h3>Quick links</h3>
            <ul class="uk-nav">
                <li><a href="">Announcements</a></li>
                <li><a href="">News</a></li>
                <li><a href="">Library</a></li>
                <li><a href="">My office</a></li>
                <li><a href="">Help</a></li>
            </ul>
          </div>
          <div class="uk-width-1-4">
            <h3>Company</h3>
            <ul class="uk-nav">
              <li><a href="">About</a></li>
              <li><a href="">Press</a></li>
              <li><a href="">Blog</a></li>
              <li><a href="">Privacy Policy</a></li>
              <li><a href="">Terms & Conditions</a></li>
            </ul>
          </div>
          <div class="uk-width-1-4">
            <h3>Contact us</h3>
            <div class="uk-margin-top uk-margin-left">
              <p>13 First Street Fifth Avenue, <br>
              Manhattan, New York<br>
              +00 41 234 567 8901
              <p><a href="">info@vantage.com</a></p>
              <div class="soc">
                <a href=""><i class="uk-icon-twitter uk-text-contrast uk-icon-small"></i></a>&nbsp;
                <a href=""><i class="uk-icon-google-plus uk-text-contrast uk-icon-small"></i></a>&nbsp;
                <a href=""><i class="uk-icon-vk uk-text-contrast uk-icon-small"></i></a>&nbsp;
                <a href=""><i class="uk-icon-linkedin uk-text-contrast uk-icon-small"></i></a>
              </div>
            </div>
          </div>
        </div>
        <hr>
        <div class="uk-grid uk-text-muted">
          <div class="uk-width-1-2">
            &copy; Vantage
          </div>
          <div class="uk-width-1-2 uk-text-right">
            Degigned and developed by <a href="https://www.jbs.com.ua/">jbs.com.ua</a>
          </div>
        </div>
      </div>
    </footer>

    <div id="filters" class="uk-modal">
      <div class="uk-modal-dialog">
        <a class="uk-modal-close uk-close"></a>
        <iframe id="searchForm" src="" width="100%" height="518px" align="center"></iframe>
      </div>
    </div>

    <script src="${baseUrl}/assets/js/common.min.js"></script>
    <script>
    window.$ || document.write('<script src="${baseUrl}/bower_components/jquery/dist/jquery.min.js"><\/script>')
    </script>
    <script>
    var baseUrl = "${baseUrl}"

    function urlChange(url) {
      $("#searchForm").attr("src", url);
    }

    var lastScrollTop = 0;
    $(document).scroll(function(event){
       var st = $(this).scrollTop();
       if (st > lastScrollTop && st != 0 && lastScrollTop != 0){
    	   $('body').stop();
       }
       lastScrollTop = st;
    });
    
    </script>
    <script src="${baseUrl}/assets/js/uikit_custom.min.js"></script>
    <script src="${baseUrl}/assets/js/altair_admin_common.min.js"></script>

    <script src='${baseUrl}/bower_components/moment/locale/ru.js'></script>
    <script src='${baseUrl}/bower_components/moment/locale/de.js'></script>
    <script src='${baseUrl}/bower_components/kalendae/build/kalendae.min.js'></script>
    <script src='${baseUrl}/bower_components/sockjs-client/dist/sockjs.min.js'></script>
    <script src='${baseUrl}/bower_components/stomp-websocket/lib/stomp.min.js'></script>

    <script src="${baseUrl}/js/base.angular.visualUpdate.js"></script>
    <script src="${baseUrl}/js/ui-notifications/ui-notifications.js"></script>
    <script src="${baseUrl}/js/common.js"></script>
    <@scripts/>
  </body>

  </html>
</#macro>