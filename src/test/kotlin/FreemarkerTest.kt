import freemarker.template.Configuration
import freemarker.template.DefaultObjectWrapper
import freemarker.template.DefaultObjectWrapperBuilder
import freemarker.template.Template
import org.junit.Assert
import org.junit.Test
import ua.ssenko.freemarker.CompilerApplication
import ua.ssenko.freemarker.TemplateCompiler
import ua.ssenko.freemarker.utils.JavaCodeCompiler
import ua.ssenko.freemarker.utils.callMethod
import ua.ssenko.freemarker.utils.getJavaName
import java.io.File
import java.io.IOException
import java.io.StringReader
import java.io.StringWriter
import java.util.*

/**
 * Created by ssenko on 7/22/16.
 */
class FreemarkerTest {


    @Test
    fun testOneTemplate() {
        testOneTemplate(File("./src/test/resources/templates/expression/assignSimpleTest.ftl"));
    }

    @Test
    fun test() {
        getAllTestTemplates().forEach {
            testOneTemplate(it)
        }
    }

    @Test
    fun howItWorksInFreemarker() {
        val it = File("./src/test/resources/templates/expression/assignWithInclude/someAssignName.ftl")
        val compiler = TemplateCompiler()
        val source = getFile(it)
        val template = loadTemplate(it.name, source)

        val fmTemplate = createFreeMarkerTemplate(it.name, source)
        val result = StringWriter();
        val startTime2 = System.nanoTime()
        template.process(null, result)
        val endTime2 = System.nanoTime()
        val originalResult = result.toString()
        println("${it.name} origin time |${(endTime2 - startTime2) / 1000}|")
        println("${it.name} originalResult ${originalResult}")
    }

    private fun testOneTemplate(it: File) {
        val compiler = TemplateCompiler()
        val source = getFile(it)
        val template = loadTemplate(it.name, source)

        val compilerJavaResult = compiler.compile(template)
        println("compilerJavaResult ${compilerJavaResult}")
        val instance = JavaCodeCompiler.compileJava(template.getJavaName(), compilerJavaResult)
        val compilerResult = instance.callMethod("generate");
        println("${it.name} compilerResult ${compilerResult}")

        val fmTemplate = createFreeMarkerTemplate(it.name, source)
        val result = StringWriter();
        val startTime2 = System.nanoTime()
        template.process(null, result)
        val endTime2 = System.nanoTime()
        val originalResult = result.toString()
        println("${it.name} origin time |${(endTime2 - startTime2) / 1000}|")
        println("${it.name} originalResult ${originalResult}")

        Assert.assertEquals(compilerResult, originalResult)
    }

    fun getAllTestTemplates() : List<File> {
        val testFileS : MutableList<File> = ArrayList();
        val file = File("./src/test/resources/templates/")

        visitDir(file, testFileS)
        testFileS.forEach { println(it.name) }
        return testFileS
    }

    private fun visitDir(file: File, testFileS: MutableList<File>) {
        file.listFiles().forEach {
            if (it.isDirectory) {
                visitDir(it, testFileS)
            } else if (it.name.contains("test") && it.name.endsWith(".ftl")) {
                testFileS.add(it)
            }
        }
    }

    @Throws(IOException::class)
    fun loadTemplate(templateName: String, source: String): Template {
        val cfg = Configuration(Configuration.VERSION_2_3_23)
        //cfg.setRegisteredCustomOutputFormats()
        cfg.objectWrapper = DefaultObjectWrapper(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS)

        val file = File("./src/test/resources/templates/")

        cfg.setDirectoryForTemplateLoading(file)

        val t = Template(templateName, StringReader(source), cfg)
        return t
    }

    fun getFile(file: File): String {

        val result = StringBuilder("")

        Scanner(file).use { scanner ->

            while (scanner.hasNextLine()) {
                val line = scanner.nextLine()
                result.append(line).append("\n")
            }

            scanner.close()

        }

        return result.toString()
    }

    fun createFreeMarkerTemplate(templateName: String, source: String):Template{
        val cfg = Configuration(Configuration.getVersion());
        cfg.setObjectWrapper(DefaultObjectWrapperBuilder(Configuration.getVersion()).build());
        return Template(templateName, StringReader(source), cfg);
    }

}