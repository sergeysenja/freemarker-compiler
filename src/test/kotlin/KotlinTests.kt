import freemarker.core.*
import org.junit.Test
import org.reflections.Reflections
import ua.ssenko.freemarker.core.CompilerConfig
import ua.ssenko.freemarker.core.NodeProcessor
import ua.ssenko.freemarker.utils.countStartSpaces
import ua.ssenko.freemarker.utils.getJavaName
import ua.ssenko.freemarker.utils.hashToString
import ua.ssenko.freemarker.utils.trimMultilineStartSpaces
import java.io.File
import java.lang.reflect.Modifier

/**
 * Created by Сегрей on 10.07.2016.
 */
class KotlinTests {

    @Test
    fun expression() {
        val e = ProxyClass.StringLiteral
        var classContent = """
            package freemarker.core

            import freemarker.core.*
            import ua.ssenko.freemarker.core.CompilationContext
            import ua.ssenko.freemarker.core.ExpressionProcessor
            import java.util.*

            /**
             * Created by Сегрей on 17.07.2016.
             */
            internal class ${e.simpleName}Processor : ExpressionProcessor<${e.name}>() {

                override fun process(context: CompilationContext, expression: ${e.name}) {
                    throw UnsupportedOperationException("not implemented")
                }

                override fun getFields(): Set<String> {
                    val fields = HashSet<String>();
            """

        e.declaredFields.forEach {
            classContent += """
                    fields.add("${it.name}");
                """
        }

        classContent += """
                    return fields;
                }

            }
            """

        println(classContent);
    }

    //@Test
    fun test() {
        val reflections = Reflections()
        val allClasses = reflections.getSubTypesOf(Expression::class.java)
        allClasses.filter {
            it.superclass.equals(Expression::class.java)
            && !Modifier.isAbstract(it.getModifiers())
        }.forEach { e ->

            var classContent = """
            package freemarker.core

            import freemarker.core.*
            import ua.ssenko.freemarker.core.CompilationContext
            import ua.ssenko.freemarker.core.ExpressionProcessor
            import java.util.*

            /**
             * Created by Сегрей on 17.07.2016.
             */
            internal class ${e.simpleName}Processor : ExpressionProcessor<${e.name}>() {

                override fun process(context: CompilationContext, expression: ${e.name}) {
                    throw UnsupportedOperationException("not implemented")
                }

                override fun getFields(): Set<String> {
                    val fields = HashSet<String>();
            """

            e.declaredFields.forEach {
                classContent += """
                    fields.add("${it.name}");
                """
            }

            classContent += """
                    return fields;
                }

            }
            """

            println(classContent);

            println("\n\n========================\n\n");

            println(classContent.trimMultilineStartSpaces());

            val dir = File("/tmp/expressions/");
            dir.mkdirs()
            val out = File("/tmp/expressions/${e.simpleName}Processor.kt");
            out.writeText(classContent.trimMultilineStartSpaces())
        }

        reflections.getSubTypesOf(Expression::class.java).filter {
            it.superclass.equals(Expression::class.java)
            && Modifier.isAbstract(it.getModifiers())
        }.forEach { e ->

            var classContent = """
            package freemarker.core

            import freemarker.core.*
            import ua.ssenko.freemarker.core.CompilationContext
            import ua.ssenko.freemarker.core.ExpressionProcessor
            import java.util.*

            /**
             * Created by Сегрей on 17.07.2016.
             */
            internal class ${e.simpleName}Processor<T: ${e.name}> : ExpressionProcessor<T>() {

                override fun process(context: CompilationContext, expression: T) {
                    throw UnsupportedOperationException("not implemented")
                }

                override fun getFields(): Set<String> {
                    val fields = HashSet<String>();
            """

            e.declaredFields.forEach {
                classContent += """
                    fields.add("${it.name}");
                """
            }

            classContent += """
                    return fields;
                }

            }
            """

            println(classContent);

            println("\n\n========================\n\n");

            println(classContent.trimMultilineStartSpaces());

            val dir = File("/tmp/expressions/");
            dir.mkdirs()
            val out = File("/tmp/expressions/${e.simpleName}Processor.kt");
            out.writeText(classContent.trimMultilineStartSpaces())
        }

        reflections.getSubTypesOf(Expression::class.java).filter {
            !it.superclass.equals(Expression::class.java)
            && Modifier.isAbstract(it.getModifiers())
        }.forEach { e ->

            var classContent = """
            package freemarker.core

            import freemarker.core.*
            import ua.ssenko.freemarker.core.CompilationContext
            import ua.ssenko.freemarker.core.ExpressionProcessor
            import java.util.*

            /**
             * Created by Сегрей on 17.07.2016.
             */
            internal class ${e.simpleName}Processor<T: ${e.name}> : ${e.superclass.name}<T>() {

                override fun process(context: CompilationContext, expression: T) {
                    throw UnsupportedOperationException("not implemented")
                }

                override fun getFields(): Set<String> {
                    val fields = HashSet<String>();
            """

            e.declaredFields.forEach {
                classContent += """
                    fields.add("${it.name}");
                """
            }

            classContent += """
                    return fields;
                }

            }
            """

            println(classContent);

            println("\n\n========================\n\n");

            println(classContent.trimMultilineStartSpaces());

            val dir = File("/tmp/expressions/");
            dir.mkdirs()
            val out = File("/tmp/expressions/${e.simpleName}Processor.kt");
            out.writeText(classContent.trimMultilineStartSpaces())
        }


        reflections.getSubTypesOf(Expression::class.java).filter {
            !it.superclass.equals(Expression::class.java)
            && !Modifier.isAbstract(it.getModifiers())
        }.forEach { e ->

            var classContent = """
            package freemarker.core

            import freemarker.core.*
            import ua.ssenko.freemarker.core.CompilationContext
            import ua.ssenko.freemarker.core.ExpressionProcessor
            import java.util.*

            /**
             * Created by Сегрей on 17.07.2016.
             */
            internal abstract class ${e.simpleName}Processor : ${e.superclass.name}<${e.name}>() {

                override fun process(context: CompilationContext, expression: ${e.name}) {
                    throw UnsupportedOperationException("not implemented")
                }

                override fun getFields(): Set<String> {
                    val fields = HashSet<String>();
            """

            e.declaredFields.forEach {
                classContent += """
                    fields.add("${it.name}");
                """
            }

            classContent += """
                    return fields;
                }

            }
            """

            println(classContent);

            println("\n\n========================\n\n");

            println(classContent.trimMultilineStartSpaces());

            val dir = File("/tmp/expressions/");
            dir.mkdirs()
            val out = File("/tmp/expressions/${e.simpleName}Processor.kt");
            out.writeText(classContent.trimMultilineStartSpaces())
        }

    }

    @Test
    fun testHash() {
        println(hashToString(-1234567890));
    }

}

